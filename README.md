
`iceberg`: a simulator for order books.
Supports "iceberg" orders.

# Usage

## Linux

A line of input:

`./run.sh <<< "hello world" 2> /dev/null`

A file of input:

`./run.sh < file 2> /dev/null`

A heredoc:

```
./run.sh << EOF
hello world!
foobar
EOF
```

# Docker (Volume mount needed)

`./rundocker.sh`
