#!/usr/bin/env bash

set -euxo pipefail

docker run --rm -it -v $(pwd):/code -w /code openjdk:9 \
    bash -c "./run.sh < examples/2.in"  # 2> /dev/null"
