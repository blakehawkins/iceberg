package com.gitlab.blakehawkins.iceberg;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.StringBuilder;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.Scanner;
import java.util.TreeSet;


final class ExchangeOrder implements Comparable<ExchangeOrder> {
    final private static Pattern rex_iceberg = Pattern.compile(
        "(S|B),(\\d+),(\\d+),(\\d+)(,(\\d+))?"
    );

    final private static Optional<Integer> maybe_int(String repr) {
        try { return Optional.of(Integer.parseInt(repr)); }
        catch (NumberFormatException e) { return Optional.empty(); }
    }

    static Optional<ExchangeOrder> from_input_repr(String line) {
        final Matcher matcher = rex_iceberg.matcher(line);

        if (matcher.find()) {
            final int quantity = Integer.parseInt(matcher.group(4));

            return Optional.of(new ExchangeOrder(
                                matcher.group(1).equals("B"),
                                Integer.parseInt(matcher.group(2)),
                                Short.parseShort(matcher.group(3)),
                                quantity,
                                maybe_int(matcher.group(5)).orElse(quantity)));
        }

        return Optional.empty();
    }

    // Ctor for normal limit order.
    private ExchangeOrder(boolean is_buy,
                          int order_id,
                          short price_pence,
                          int quantity) {
        // A normal limit order is just an iceberg order with equal peak size
        // and quantity.
        this(is_buy, order_id, price_pence, quantity, quantity);
    }

    // Ctor for iceberg limit order.
    private ExchangeOrder(boolean is_buy,
                          int order_id,
                          short price_pence,
                          int quantity,
                          int peak_size) {
        this._is_buy = is_buy;
        this._order_id = order_id;
        this._price_pence = price_pence;
        this._quantity = quantity;
        this._peak_size = peak_size;
        this._timestamp = System.nanoTime();
    }

    int consume(int try_amount) {
        throw new java.lang.UnsupportedOperationException();
    }

    short get_price() {
        return this._price_pence;
    }

    boolean is_buy() {
        return this._is_buy;
    }

    final private static DecimalFormat dec_fmt = new DecimalFormat("#,###");

    // Get just one side of the output table, depending on whether this is a
    // buy or a sell.
    String repr_fragment() {
        if (_is_buy) {
            return String.format("%10d|%13s|%7s",
                                 _order_id,
                                 dec_fmt.format(_quantity),
                                 dec_fmt.format(_price_pence));
        }

        return String.format("%7s|%13s|%10d",
                             dec_fmt.format(_price_pence),
                             dec_fmt.format(_quantity),
                             _order_id);
    }

    private boolean _is_buy;
    private int _order_id;
    private short _price_pence;
    private long _timestamp;
    private int _quantity;
    private int _peak_size;

    @Override
    public final int compareTo(ExchangeOrder other) {
        int ord_1 = Short.compare(this._price_pence, other._price_pence);

        return ord_1 == 0
            ? Long.compare(this._timestamp, other._timestamp)
            : ord_1;
    }
}

enum ReconcileResult {
    FULL,    // The order was fully reconciled (consumed).
    PARTIAL  // Some or none of the order matched against the book.
}

public class Main {
    private static final <T> Optional<T> maybeNext(Iterator<T> iter) {
        if (iter.hasNext()) {
            return Optional.of(iter.next());
        }

        return Optional.empty();
    }

    private static final String repeat_char(char which, int count) {
        final StringBuilder builder = new StringBuilder();
        IntStream.range(0, count).forEach(_x -> builder.append(which));

        return builder.toString();
    }

    private static final String hr = new StringBuilder()
        .append('+')
        .append(repeat_char('-', 65))
        .append('+')
        .append('\n')
        .toString();

    private static final String buy_sell_text = new StringBuilder()
        .append('|')
        .append(' ')
        .append("BUY")
        .append(repeat_char(' ', 28))
        .append('|')
        .append(' ')
        .append("SELL")
        .append(repeat_char(' ', 27))
        .append('|')
        .append('\n')
        .toString();

    private static final String headings = new StringBuilder()
        .append('|')
        .append(' ')
        .append("Id")
        .append(repeat_char(' ', 7))
        .append('|')
        .append(' ')
        .append("Volume")
        .append(repeat_char(' ', 6))
        .append('|')
        .append(' ')
        .append("Price")
        .append(' ')
        .append('|')
        .append(' ')
        .append("Price")
        .append(' ')
        .append('|')
        .append(' ')
        .append("Volume")
        .append(repeat_char(' ', 6))
        .append('|')
        .append(' ')
        .append("Id")
        .append(repeat_char(' ', 7))
        .append('|')
        .append('\n')
        .toString();

    private static final String underline = new StringBuilder()
        .append('+')
        .append(repeat_char('-', 10))
        .append('+')
        .append(repeat_char('-', 13))
        .append('+')
        .append(repeat_char('-', 7))
        .append('+')
        .append(repeat_char('-', 7))
        .append('+')
        .append(repeat_char('-', 13))
        .append('+')
        .append(repeat_char('-', 10))
        .append('+')
        .append('\n')
        .toString();

    private static final String buy_empty = new StringBuilder()
        .append(repeat_char(' ', 10))
        .append('|')
        .append(repeat_char(' ', 13))
        .append('|')
        .append(repeat_char(' ', 7))
        .toString();

    private static final String sell_empty = new StringBuilder(buy_empty)
        .reverse()
        .toString();

    public static String format_order_book(TreeSet<ExchangeOrder> buys,
                                           TreeSet<ExchangeOrder> sells) {
        final StringBuilder builder = new StringBuilder();

        builder.append(hr);
        builder.append(buy_sell_text);
        builder.append(headings);
        builder.append(underline);

        final Iterator<ExchangeOrder> buy_iter  =  buys.iterator();
        final Iterator<ExchangeOrder> sell_iter = sells.iterator();

        while (buy_iter.hasNext() || sell_iter.hasNext()) {
            builder.append('|');
            builder.append(
                maybeNext(buy_iter)
                    .map(x -> x.repr_fragment())
                    .orElse(buy_empty)
            );
            builder.append('|');
            builder.append(
                maybeNext(sell_iter)
                    .map(x -> x.repr_fragment())
                    .orElse(sell_empty)
            );
            builder.append('|');
            builder.append('\n');
        }

        builder.append(hr);

        return builder.toString();
    }

    private boolean _is_buy;
    private int _order_id;
    private short _price_pence;
    private long _timestamp;
    private int _quantity;
    private int _peak_size;


    public static ReconcileResult reconcile(
        ExchangeOrder order,
        TreeSet<ExchangeOrder> book
    ) {
        final Iterator<ExchangeOrder> iter = book.iterator();

        book
            .stream()
            .filter(o -> o.get_price() <= order.get_price())
            .forEach(o -> {
                ;
            });
        return ReconcileResult.PARTIAL;
    }

    public static void main(String[] args) {
        final TreeSet<ExchangeOrder> buys  = new TreeSet<>();
        final TreeSet<ExchangeOrder> sells = new TreeSet<>();

        new BufferedReader(new InputStreamReader(System.in))
            .lines()
            .forEach(line -> {
                ExchangeOrder.from_input_repr(line)
                             .stream()
                             .forEach(order -> {
                    final ReconcileResult result = reconcile(
                        order,
                        order.is_buy() ? sells : buys
                    );

                    if (result != ReconcileResult.FULL) {
                        (order.is_buy() ? buys : sells).add(order);
                    }

                    System.out.println(format_order_book(buys, sells));
                });
            });
    }
}
