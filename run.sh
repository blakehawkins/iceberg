#!/usr/bin/env bash

set -euxo pipefail

mkdir -p build

javac -Xlint:deprecation -Xlint:unchecked -d ./build src/main/java/com/gitlab/blakehawkins/iceberg/*.java > /dev/null

pushd build > /dev/null

jar cfe iceberg.jar com/gitlab/blakehawkins/iceberg/Main * > /dev/null

popd > /dev/null

java -jar build/iceberg.jar <&0
